# Функция list_count_num. Принимает 2 аргумента:
# список числами my_list (который создал пользователь) и число num.
# Возвращает количество num в списке my_list


def list_count_num(my_list, num):
    return my_list.count(num)


def main():
    lst_str = input('Введите список с числами через запятую (без пробелов): ')
    lst = lst_str.split(',')
    num = input('Введите число: ')
    print(list_count_num(lst, num))


if __name__ == '__main__':
    main()
