from flask import request
from flask import Flask
from flask import render_template

# Передаем имя модуля или пакета
# Это необходимо, так как Flask не знает, где искать шаблоны, статические файлы, и т.п.
app = Flask(__name__)

my_test_dict = {
    'big': {
        'elephant': 3,
        'whale': 1
    },
    'medium': {
        'lion': 66,
        'tiger': 22
    },
    'small': {
        'bird': 888,
        'cockroach': 9999
    }
}


# http://localhost:5000/goods
@app.route('/goods', methods=['GET', 'POST'])
def goods():
    if request.method == 'POST':
        if 'add' in request.form:
            my_test_dict.update(
                {
                    request.form.get('category'): {
                        request.form.get('name'): int(request.form.get('quantity'))
                    }
                }
            )

        elif 'save' in request.form:
            my_test_dict[request.form.get('category')][request.form.get('name')] = int(request.form.get('quantity'))

        elif 'delete' in request.form:
            del my_test_dict[request.form.get('category')][request.form.get('name')]

    return render_template('goods.html', dict=my_test_dict)


if __name__ == "__main__":
    # Запуск локального сервера с приложением
    app.run(debug=True)
