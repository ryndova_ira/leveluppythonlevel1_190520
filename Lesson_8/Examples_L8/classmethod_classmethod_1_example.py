class MyClass:
    def instancemethod(self):
        return 'instance method called', self

    @classmethod
    def classmethod(cls):
        return 'class method called', cls

    @staticmethod
    def staticmethod():
        return 'static method called'


obj = MyClass()
print(obj.instancemethod())         # ('instance method called', <__main__.MyClass object at 0x7f36bbd98a10>)
print(MyClass.instancemethod(obj))  # ('instance method called', <__main__.MyClass object at 0x7f36bbd98a10>)
# print(MyClass.instancemethod())   # TypeError: instancemethod() missing 1 required positional argument: 'self'

print(obj.classmethod())            # ('class method called', <class '__main__.MyClass'>)
print(MyClass.classmethod())        # ('class method called', <class '__main__.MyClass'>)

print(obj.staticmethod())           # static method called
print(MyClass.staticmethod())       # static method called
# ---------------------------------------------------------------------------------------------------------------------

print("welcome".upper())  # метод экземпляра класса

print(dict.fromkeys('AEIOU'))  # метод класса dict
