my_tuple = (1, True, [1, 2, 3])
my_tuple_2 = (1, True, [1, 2, 3])

my_tuple[2].append(6)
print(my_tuple)

my_list = [1, True, [1, 2, 3]]
my_list_2 = [1, True, [1, 2, 3]]
my_list[2].append(6)

print(my_tuple == my_list)
print(my_tuple == my_tuple_2)
print(my_list == my_list_2)

del my_tuple[2][1]
print(my_tuple)


