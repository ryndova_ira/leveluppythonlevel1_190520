# На словари можно простенькое: телефонная книжка.
# Ключи - имена. Значение - листы телефонов
# (у одного человека может быть больше одного телефона).
# Найти все телефоны по имени. И найти имя по телефону.


def get_numbers_by_name(names_with_numbers, name):
    return names_with_numbers.get(name)


def get_name_by_number(names_with_numbers, num):
    for k, val in names_with_numbers.items():
        if num in val:
            return k

    return 'No name'


def main():
    names_with_numbers = {'Anton': [111, 222, 444],
                          'Kate': [123, 555],
                          'Petr': [777]}
    print(get_numbers_by_name(names_with_numbers, 'Petr'))
    print(get_numbers_by_name(names_with_numbers, 'Anton'))
    print(get_numbers_by_name(names_with_numbers, 'Anonymous'))
    print(get_name_by_number(names_with_numbers, 777))
    print(get_name_by_number(names_with_numbers, 666))


if __name__ == '__main__':
    main()
