import matplotlib.pyplot as plt

num = 20
t_1 = [i for i in range(num)]
t_2 = [i ** 2 for i in range(num)]
t_3 = [i ** 3 for i in range(num)]

# red dashes, blue squares and green triangles
plt.plot(t_1, t_1, 'r--*', t_1, t_2, 'bs', t_1, t_3, 'g^-')
plt.show()
