def append_default(element, lst=[]):
    lst.append(element)
    return lst


print(append_default("1"))  # ['1']

print(append_default("2"))  # ['1', '2']
