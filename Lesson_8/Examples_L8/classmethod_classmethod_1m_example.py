class MyClass:
    class_state = '234'

    def __init__(self):
        self.obj_state = '123'

    def instancemethod(self):
        return 'instance method called', self.class_state, self.__class__.class_state, self.obj_state, self

    def change_class_state(self, string):
        self.class_state += string
        self.__class__.class_state += string*2

    @classmethod
    def classmethod(cls):
        return 'class method called', cls.class_state, cls

    @staticmethod
    def staticmethod(my_class):
        return 'static method called', my_class.class_state


obj = MyClass()
obj.change_class_state('q')
print(obj.instancemethod())         # ('instance method called', <__main__.MyClass object at 0x7f36bbd98a10>)
print(MyClass.instancemethod(obj))  # ('instance method called', <__main__.MyClass object at 0x7f36bbd98a10>)
# print(MyClass.instancemethod())   # TypeError: instancemethod() missing 1 required positional argument: 'self'

print(obj.classmethod())            # ('class method called', <class '__main__.MyClass'>)
print(MyClass.classmethod())        # ('class method called', <class '__main__.MyClass'>)

print(obj.staticmethod(obj))           # static method called
print(MyClass.staticmethod(obj))       # static method called
# ---------------------------------------------------------------------------------------------------------------------

print("welcome".upper())  # метод экземпляра класса

print(dict.fromkeys('AEIOU'))  # метод класса dict
