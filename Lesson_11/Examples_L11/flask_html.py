from flask import request
from flask import Flask
from flask import render_template

# Передаем имя модуля или пакета
# Это необходимо, так как Flask не знает, где искать шаблоны, статические файлы, и т.п.
app = Flask(__name__)


def do_the_login():
    pass


def show_the_login_form():
    pass


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        do_the_login()
    else:
        show_the_login_form()


# Example: http://localhost:5000/hello/my_user или http://localhost:5000/hello
@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)


if __name__ == "__main__":
    # Запуск локального сервера с приложением
    app.run(debug=True)
