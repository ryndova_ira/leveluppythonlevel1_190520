# Функция gen_list_double_start_stop. Принимает 2 аргумента: числа start, stop.
# Возвращает список состоящий из удвоенных значений от start до stop (не включая).
# Пример: start=3, stop=6, результат [6, 8, 10].


def gen_list_double_start_stop(start, stop):
    return [x*2 for x in range(start, stop)]


def main():
    start_lst = [1, 5, 7, 9, 11]
    stop_lst = [33, 10, -5, 11, 4]
    for x, y in zip(start_lst, stop_lst):
        print(gen_list_double_start_stop(x, y))


if __name__ == '__main__':
    main()
