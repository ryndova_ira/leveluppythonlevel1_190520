class Person:

    def __init__(self, first, last, age):
        self.firstname = first
        self.lastname = last
        self.age = age

    def __str__(self):
        return self.firstname + " " + self.lastname + ", " + str(self.age)


class Employee(Person):

    def __init__(self, first, last, age, staff_num):
        super().__init__(first, last, age)
        self.staff_number = staff_num

    def __str__(self):
        return super().__str__() + ", " + self.staff_number

    def str_parent(self):
        return super().__str__()


p = Person("Ivan", "Ivanov", 36)
e = Employee("Petr", "Petrov", 28, "1007")


print(p)    # Ivan Ivanov, 36
print(e)    # Petr Petrov, 28, 1007
print(e.str_parent())   # Petr Petrov, 28
