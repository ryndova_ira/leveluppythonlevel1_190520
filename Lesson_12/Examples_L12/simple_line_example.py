import matplotlib.pyplot as plt

fig, ax = plt.subplots()  # Create a figure containing a single axes.
ax.plot([1, 2, 3, 4], [5, -5, 0, 3])  # Plot some data on the axes.
plt.show()
