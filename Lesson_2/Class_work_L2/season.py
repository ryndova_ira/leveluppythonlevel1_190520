# Функция season. Принимает 1 аргумент — номер месяца (от 1 до 12),
# и возвращает время года,
# которому этот месяц принадлежит (зима, весна, лето или осень)


def season(num):
    if num == 1 or num == 2 or num == 12:
        return "Winter"
    elif 3 <= num <= 5:
        return "Spring"
    elif 6 <= num <= 8:
        return "Summer"
    elif 9 <= num <= 11:
        return "Fall"


def main():
    while True:
        in_str = input('Введите номер месяца (от 1 до 12). Введите 0, если конец:')
        if not in_str.isdigit():
            print(f"Ошибка входных данных: {in_str}. Введите число.")
        elif int(in_str) > 12:
            print(f"Ошибка. Месяца с таким числом ({in_str}) не существует! ")
        else:
            number = int(in_str)
            if number == 0:
                break
            print(season(number))


if __name__ == '__main__':
    main()
