print('\n--------------------------------------- Introduction --------------------------------------------------------')

# Синтаксис описания словаря: список пар ключ: значения в фигурных скобках через запятую
d = {1: 'one', 2: 'two', 3: 'three'}
print(d)  # {1: 'one', 2: 'two', 3: 'three'}

# Синтаксис добавления элемента
d[4] = 'four'
print(d)  # {1: 'one', 2: 'two', 3: 'three', 4: 'four'}

# Синтаксис обращения к значению по ключу
print(d[1])  # 'one'

print('\n--------------------------------------- For -----------------------------------------------------------------')
my_dict = {1: 'one', 2: 'two', 3: 'three'}
for key, value in my_dict.items():
    print(f'key: {key} | value: {value}')

for key in my_dict.keys():
    print(f'key: {key}')

for value in my_dict.values():
    print(f'value: {value}')

print('\n--------------------------------------- Dict Comprehension --------------------------------------------------')

dc_1 = {n: n ** 2 for n in range(5)}
print(dc_1)
# {0: 0, 1: 1, 2: 4, 3: 9, 4: 16}

dc_2 = {n: True for n in range(5)}
print(dc_2)
# {0: True, 1: True, 2: True, 3: True, 4: True}

lst = ['hello', 'hi', 'hello', 'today', 'morning', 'again', 'hello']
dc_3 = {k: lst.count(k) for k in lst}
print(dc_3)
# {'hello': 3, 'hi': 1, 'today': 1, 'morning': 1, 'again': 1}
