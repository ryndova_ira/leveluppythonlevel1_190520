# Синтаксис:
# class <ClassName>(<ClassNames>):
#   statements


class MyClass():
    # Конструктор (фабрика, которая создаёт экземпляры)
    def __init__(self, n):
        # Поле (атрибут) name
        self.name = n

    # def method(self[, args]):
    #   statements
    def print_name(self):
        print(self.name)


# class создаёт класс (объект-класс)
print(MyClass)
# <class '__main__.MyClass'>

# Создание экземпляра класса
# инстанцирование (англ. instantiation)
instance = MyClass("MyObject")
print(instance)
# <__main__.MyClass object at 0x7fc1ae739f50>

print(instance.name)  # MyObject

instance.print_name()  # MyObject
