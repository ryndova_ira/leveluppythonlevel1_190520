# Функция zip_name_mark. Принимает 2 аргумента: список с именами и список с оценкой (от 1 до 5).
# Возвращает список с парами значений из каждого аргумента, если один список больше другого, то
# заполнить недостающие элементы строкой “!!!”. (* Подсказка: zip_longest)
from itertools import zip_longest


def zip_name_mark(names, marks):
    return zip_longest(names, marks, fillvalue='!!!')


def main():
    names = ['a', 'b', 'c', 'd']
    marks = [3, 5]
    print(list(zip_name_mark(names, marks)))


if __name__ == '__main__':
    main()