from flask import Flask, render_template, request

# Передаем имя модуля или пакета
# Это необходимо, так как Flask не знает, где искать шаблоны, статические файлы, и т.п.

app = Flask(__name__)


@app.route('/login/', methods=['post', 'get'])
def login():
    message = ''
    if request.method == 'POST':
        username = request.form.get('username')  # запрос к данным формы
        password = request.form.get('password')

        if username == 'root' and password == 'pass':
            message = "Correct username and password"
        else:
            message = "Wrong username or password"

    return render_template('login.html', message=message)


if __name__ == "__main__":
    # Запуск локального сервера с приложением
    app.run(debug=True)
