def mylog(func):
    import time

    def wrapper(self, *args):
        print("Я получил: ", args)
        start = time.time_ns()
        try:
            func(self, *args)
        except Exception as ex:
            print(f"Exception (decorator): {ex}")
        else:
            print("OK")
        finally:
            end = time.time_ns()
            print('Время выполнения: {} наносекунд.'.format(end - start))

    return wrapper


class Mine(object):

    def __init__(self):
        self._x = 'some value'

    @property
    @mylog
    def prop(self):
        return self._x


def main():
    print(type(Mine.prop))
    # <class 'property'>

    print(help(Mine.prop))
    # Help on property: None

    mine = Mine()
    print(mine.prop)  # some value

    mine.prop = 'other value'
    # AttributeError: can't set attribute

    del mine.prop
    # AttributeError: can't delete attribute


if __name__ == '__main__':
    try:
        main()
    except Exception as ex:
        print(ex)
    else:
        print("Everything was OK")
    finally:
        print("Thanks!!!")
