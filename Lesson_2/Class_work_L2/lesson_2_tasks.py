# Функция check_plus_last_num. Принимает двузначное число.
# Вернуть результат вычисления выражения: число + (последняя цифра числа)


def check_plus_last_num(num):
    if len(str(num)) == 2 and str(num).isdigit():      # Двузначное? и Число?
        # Получить последнюю цифру
        last_num = str(num)[-1]
        # Вернуть число + (последняя цифра числа)
        return int(num) + int(last_num)
    else:
        return None


def def_many_arg(a, b, c, d=3837):
    print(f"{a}, {b}, {c}, {d}")


if __name__ == '__main__':
    def_many_arg(b=5, c='88', a=7)
    number = input('Введите двузначное число:')
    print(check_plus_last_num(num=number))

# number = input('Введите двузначное число:')
# print(check_plus_last_num(num=number))