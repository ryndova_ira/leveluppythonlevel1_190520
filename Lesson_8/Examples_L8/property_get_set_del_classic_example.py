class Mine(object):
    """
    Класс для тестов
    """

    def __init__(self):
        self._x = None

    def get_x(self):
        return self._x

    def set_x(self, value):
        self._x = value * 5

    def del_x(self):
        self._x = 'No more'

    x = property(fget=get_x, fset=set_x, fdel=del_x,
                 doc='Это свойство x.')

print(help(Mine))
print(type(Mine.x))  # <class 'property'>
print(help(Mine.x))  # Help on property: Это свойство x.
mine = Mine()
print(mine.x)  # None
mine.x = 3
print(mine.x)  # 3
del mine.x
print(mine.x)  # No more

test_val = 5
print(test_val)
test_val = 1
del test_val
pass