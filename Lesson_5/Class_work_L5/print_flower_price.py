# Определить функцию print_flower_price.
# Принимает 2 аргумента: название цветка (строка) и неопределенное количество цен (*args).
# Функция print_flower_price выводит вначале название цветка, а потом все цены на него.
# В функции main вызывается функция print_flower_price и передается название цветка и произвольное количество цен.
# Например, print_flower_price(‘rose’, 77, 10, 50, 99) или
# print_flower_price(‘tulp’, 100, 44, 777, 876, 555, 111)


def print_flower_price(flower_name, *prices):
    price_str_lst = [str(elem) for elem in prices]
    prices_str = ' '.join(price_str_lst)
    print("{0}: {1}".format(flower_name, prices_str))
    # print(flower_name, *prices)   - тоже работает


def main():
    print_flower_price('rose', 77, 10, 50, 99)


if __name__ == '__main__':
    main()
