print('\n--------------------------------------- Introduction --------------------------------------------------------')
# Синтаксис описания простейшего кортежа: список значений в круглых скобках через запятую
ex1 = (1, 2, 3)
print(ex1)  # (1, 2, 3)

# Скобки можно опустить
ex2 = 1, 2, 3
print(ex2)  # (1, 2, 3)

# Для определения кортежа единичной длины указывается запятая без элемента
ex3 = (1,)
print(ex3)  # (1,)

# Для определения кортежа нулевой длины можно использовать пару круглых скобок
ex4 = ()
print(ex4)  # ()

# Кортежи при написании перестановкой значений
a = 1
b = 2
print(f"a = {a}; b = {b}")  # a = 1; b = 2
a, b = b, a
print(f"a = {a}; b = {b}")  # a = 2; b = 1

print('\n--------------------------------------- Tuple Comprehension --------------------------------------------------')

tuple_1 = tuple(n ** n for n in range(5))
print(tuple_1)
# (1, 1, 4, 27, 256)

tuple_2 = tuple(n ** 2 for n in (1, 33, 3, 444, 5, 6, 999) if n % 2 == 0)
print(tuple_2)
# (197136, 36)

lst = ['hello', 'hi', 'hello', 'today', 'morning', 'again', 'hello']
tuple_3 = tuple(lst.count(k) * k
                for k
                in lst)
print(tuple_3)
# ('hellohellohello', 'hi', 'hellohellohello', 'today', 'morning', 'again', 'hellohellohello')