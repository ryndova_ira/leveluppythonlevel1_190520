from PyQt5 import QtWidgets, uic
import sys

app = QtWidgets.QApplication([])
# win = uic.loadUi("main.ui")     # расположение файла .ui
win = uic.loadUi("class_main.ui")     # расположение файла .ui

win.show()
sys.exit(app.exec())