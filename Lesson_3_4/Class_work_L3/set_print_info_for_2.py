# Функция set_print_info_for_2. Принимает 2 аргумента:
# множества “ассорти” my_set_left и my_set_right (которые создал пользователь).
# Выводит информацию о: равенстве множеств, имеют ли они общие элементы,
# является ли my_set_left подмножеством my_set_right и наоборот.


def set_print_info_for_2(my_set_left, my_set_right):
    print(f'left: {my_set_left} | right: {my_set_right}')
    print('равенство: {eq}'.format(eq=my_set_left == my_set_right))
    print('общие элементы: {inter}'.format(inter=my_set_left.intersection(my_set_right)))
    print('left подмножество right: {sub}'.format(sub=my_set_left.issubset(my_set_right)))
    print('right подмножество left: {sub}'.format(sub=my_set_right.issubset(my_set_left)))


def main():
    str1 = input('Введите элементы множества через пробелы: ')
    str2 = input('Введите элементы множества через пробелы: ')
    set1 = set(str1.split(' '))
    set2 = set(str2.split(' '))
    set_print_info_for_2(set1, set2)


if __name__ == '__main__':
    main()
