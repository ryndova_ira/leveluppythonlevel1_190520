def test(func):
    import time

    def wrapper(*args):
        print("Я получил: ", args)
        start = time.time_ns()
        func(*args)
        end = time.time_ns()
        print('Время выполнения: {} наносекунд.'.format(end - start))

    return wrapper


@test
def do_useless_work(*args):
    counter = 0
    for i in range(args[0], args[1]):
        counter += i


do_useless_work(0, 999999)
