# Определить функцию hello_with_default_vals_v1.
# Принимает 3 аргумента: имя (по умолчанию “Иван”), фамилия (по умолчанию “Иванов”) и возраст (по умолчанию 18).
# Функция hello_with_default_vals_v1 выводит строку в формате
# “Здравствуйте <имя> <фамилия>! <возраст> лет - самый лучший возраст!”.
# При этом в функции hello_with_default_vals_v1 есть проверка на то, что имя и фамилия - это строки
# (* Подсказка: type(x) == str или isinstance(s, str)), а также возраст - это число больше 0, но меньше 150.
# В функции main вызвать функцию hello_with_default_vals_v1 различными способами
# (перебрать варианты: пусто, имя, фамилия, возраст, имя фамилия, имя возраст, фамилия возраст, имя фамилия возраст).
# (* Использовать именованные аргументы)

isinstance
def hello_with_default_vals_v1(fname='Ivan', lname='Ivanov', age=18):
    # if type(fname) == str and type(lname) == str and (type(age) == int and 0 < age < 150):
    if isinstance(fname, str) and isinstance(lname, str) and (isinstance(age, int) and 0 < age < 150):
        return f"Здравствуйте {fname} {lname}! {age} лет - самый лучший возраст!"

    return None


def hello_with_default_vals_v2(fname='Ivan', lname='Ivanov', age=18):
    # if not (type(fname) == str and type(lname) == str and (type(age) == int and 0 < age < 150)):
    if type(fname) != str or type(lname) != str or (type(age) != int or age < 0 or age > 150):
        return None

    return f"Здравствуйте {fname} {lname}! {age} лет - самый лучший возраст!"


def main():
    # правильные
    print(hello_with_default_vals_v1())
    print(hello_with_default_vals_v1('Petr'))
    print(hello_with_default_vals_v1(lname='Petrov'))
    print(hello_with_default_vals_v1(age=99))
    print(hello_with_default_vals_v1('Boris', 'Borisov'))
    print(hello_with_default_vals_v1('Misha', age=38))
    print(hello_with_default_vals_v1(lname='Godunov', age=67))
    print(hello_with_default_vals_v1('Kate', 'Katerinova', age=20))

    # с ошибками
    print(hello_with_default_vals_v1(fname=77))
    print(hello_with_default_vals_v1(lname=77))
    print(hello_with_default_vals_v1(age=[]))


if __name__ == '__main__':
    main()
