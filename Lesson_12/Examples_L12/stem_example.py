import matplotlib.pyplot as plt

x = [1, 2, 3, 4, 5, 6]
y = [4, 6, 33, 10, 22, 44]

plt.stem(x, y, use_line_collection=True)
plt.show()
