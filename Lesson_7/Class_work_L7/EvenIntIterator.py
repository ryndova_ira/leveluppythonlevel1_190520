# Класс EvenIntIterator.
# Аргументы: целое число n.
# Перегрузить методы __init__, __iter__ и __next__
# таким образом чтобы при итерировании по экземпляру
# EvenIntIterator метод next возвращает следующее четное число в диапазоне [0, n),
# если произошел выход за пределы этого диапазона - ошибка StopIteration.


class EvenIntIterator:
    def __init__(self, n):
        self.n = n
        self.counter = 0

    def __next__(self):
        if self.counter % 2 != 0:
            self.counter += 1

        if self.counter < self.n:
            result = self.counter
            self.counter += 2
            return result
        else:
            raise StopIteration()

    def __iter__(self):
        return self


def main():
    even_iterator = EvenIntIterator(7)
    even_iterator.counter = 3
    print(next(even_iterator))
    print(next(even_iterator))
    print(next(even_iterator))


if __name__ == '__main__':
    main()
