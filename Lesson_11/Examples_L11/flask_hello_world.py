from flask import Flask

# Передаем имя модуля или пакета
# Это необходимо, так как Flask не знает, где искать шаблоны, статические файлы, и т.п.
app = Flask(__name__)


# Декоратор говорит Flask, что URL должен вызывать нашу функцию.
# Example: http://localhost:5000/
@app.route("/")
def hello():
    return "Hello World!"


# Example: http://localhost:5000/status - OK
# Example: http://localhost:5000/status/ - Not Found
@app.route('/status')
def status():
    return "Сделано на flask"


# Example: http://localhost:5000/projects/
@app.route('/projects/')
def projects():
    return "Мои проекты!"


# Example: http://localhost:5000/user/my_user
@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return 'User %s' % username


# <converter:variable_name>
# converter может быть int, float, path
# Example: http://localhost:5000/post/369
@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return 'Post %d' % post_id


if __name__ == "__main__":
    # Запуск локального сервера с приложением
    app.run(debug=True)
