class Page:
    # Класс Страница
    # Атрибуты: номер страницы, контент
    # Методы: вывести контент, поиск строки в контенте

    def __init__(self, num, cnt):
        self.number = num
        self.content = cnt

    def print_content(self):
        print(self.content)

    def get_num_content(self):
        return f'{self.number}: {self.content}'

    def find_str_in_content(self, string):
        return self.content.find(string)


class Book:
    # Класс Книга
    # Атрибуты: страницы, название, автор, год издания
    # Методы: вывести весь контент, добавить страницу в конец,
    #         получить количество страниц, вернуть все четные страницы
    def __init__(self, title, author, year):
        self.pages = []
        self.title = title
        self.author = author
        self.year = year

    def print_content(self):
        for page in self.pages:
            page.print_content()

    def extend(self, *pages):
        self.pages.extend(pages)

    def get_page_count(self):
        return len(self.pages)

    def get_even_pages(self):
        return [p for p in self.pages if p.number % 2 == 0]


def main():
    # Создаем книгу и заполняем книгу страницами
    book = Book(title='Колобок', author='Народное творчество', year=2020)
    page_1 = Page(num=1, cnt='Содержание')
    print(page_1.find_str_in_content('ерж'))
    book.extend(page_1)
    book.extend(Page(num=2, cnt='Введение'))
    book.extend(Page(num=3, cnt='Жили были'), Page(num=4, cnt='Хотели кушать'), Page(num=5, cnt='Съели'))
    book.print_content()
    print(book.get_page_count())
    even_pages = book.get_even_pages()
    print(even_pages)   # [<__main__.Page object at 0x000002D4F75C38B0>, <__main__.Page object at 0x000002D4F75C3A60>]
    print_none = [print(p.get_num_content()) for p in book.get_even_pages()]
    even_pages_info = [p.get_num_content() for p in book.get_even_pages()]
    print(even_pages_info)


if __name__ == '__main__':
    main()