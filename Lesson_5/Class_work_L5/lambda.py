def main():
    # lambda складывает аргументы a и b и выводит результат.
    (lambda a, b: print(a + b))(3, 4)
    # lambda возводит в 3ю степень переданный аргумент возвращает результат
    print((lambda x: x ** 3)(3))


if __name__ == '__main__':
    main()