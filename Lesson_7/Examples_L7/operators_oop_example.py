class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __add__(self, other):
        return str(other) + ' ' + self.name

    def __radd__(self, other):
        return str(other) + ' ' + self.name


dog = Dog('Rex', 2)

# __add__ и оператор (+)

# Вызов __add__ у Dog
print(dog + 'Inspector')
print(dog.__add__('Super'))     # эквивалентно
print(dog + 4)
# Inspector Rex

# Вызов __add__ у str
print('Inspector' + dog)
# TypeError: can only concatenate str (not "Dog") to str
# Иначе нужно использовать __radd__
