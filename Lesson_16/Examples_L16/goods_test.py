import pytest
from selenium import webdriver


# Fixture for Chrome
@pytest.fixture(scope="class")
def setup(request):
    print("initiating chrome driver")
    chrome_driver = webdriver.Chrome('chromedriver')
    url = "http://localhost:5000/goods"
    chrome_driver.get(url)
    chrome_driver.maximize_window()
    request.cls.driver = chrome_driver

    yield chrome_driver

    chrome_driver.close()


@pytest.mark.usefixtures("setup")
class TestExampleOne:
    def test_title(self):
        assert "Товары" in self.driver.title

    def test_title_FAIL(self):
        assert "Goods" in self.driver.title

    def test_goods_list_len(self):
        good_list = self.driver.find_elements_by_id("good_list")
        assert len(good_list) == 6

    def test_goods_list(self):
        goods_list_raw = self.driver.find_elements_by_id("good_list")
        goods_list = [x.text for x in goods_list_raw]
        assert goods_list == ['elephant', 'whale', 'lion', 'tiger', 'bird', 'cockroach']