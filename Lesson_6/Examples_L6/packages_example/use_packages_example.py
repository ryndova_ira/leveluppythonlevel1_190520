import Lesson_6.Examples_L6.packages_example.sound as sound
import Lesson_6.Examples_L6.packages_example.sound.effects.echo as echo
from Lesson_6.Examples_L6.packages_example.sound.filters import TYPE
from sound.formats import FORMAT   # Относительный импорт (не рекомендован)

print(__file__)
print(sound.NAME)
print(sound.effects.echo)
print(echo.ECHO)
print(TYPE)
print(FORMAT)
