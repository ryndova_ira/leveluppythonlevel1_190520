from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QTreeWidgetItem

from Lesson_13.Examples_L13.main_ui import Ui_MainWindow  # импорт нашего сгенерированного файла
import sys

my_test_dict = {
    'big': {
        'elephant': 3,
        'whale': 1
    },
    'medium': {
        'lion': 66,
        'tiger': 22
    },
    'small': {
        'bird': 888,
        'cockroach': 9999
    }
}

company_name = 'My Company'


class mywindow(QtWidgets.QMainWindow):
    def __init__(self):
        # super(mywindow, self).__init__()
        super().__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.addButton.clicked.connect(self.add_clicked)
        self.ui.addButton.clicked.connect(self.add_clicked_tree)
        self.ui.updateButton.clicked.connect(self.update_clicked)

        self.ui.nameLabel.setText(company_name)

        pixmap = QPixmap('image.jpg')
        self.ui.imageLabel.setPixmap(pixmap)
        self.ui.imageLabel.resize(pixmap.width(), pixmap.height())

    def tree_update(self):
        self.tree_updated.emit()

    def update_clicked(self):
        self.ui.treeWidget.clear()
        for animal_type, animal in my_test_dict.items():
            for name, count in animal.items():
                item = QTreeWidgetItem(self.ui.treeWidget)
                item.setText(0, animal_type)
                item.setText(1, name)
                item.setText(2, str(count))

    def add_clicked(self):
        for animal_type, animal in my_test_dict.items():
            for name, count in animal.items():
                item = QTreeWidgetItem(self.ui.treeWidget)
                item.setText(0, animal_type)
                item.setText(1, name)
                item.setText(2, str(count))

    def add_clicked_tree(self):
        for animal_type, animal in my_test_dict.items():
            item = QTreeWidgetItem(self.ui.treeWidget_2)
            item.setText(0, animal_type)
            type_count = 0
            for name, count in animal.items():
                item_child = QTreeWidgetItem(item)
                item_child.setText(0, name)
                item_child.setText(1, str(count))
                type_count += count

            item.setText(1, str(type_count))


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    application = mywindow()
    application.show()

    sys.exit(app.exec())
