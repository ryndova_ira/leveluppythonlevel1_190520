# Импортируем функцию hello из модуля hello
import hello
# importing hello module

import importlib

# Перезагружает ранее импортированный модуль
importlib.reload(hello)
# importing hello module
