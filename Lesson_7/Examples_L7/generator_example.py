def generator(n):
    for i in range(0, n):
        yield i


def return_fun(n):
    for i in range(0, n):
        return i


return_res = return_fun(55)

myrange = generator(10)
print(list(myrange))
# [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

for i in generator(10):
    print(i)

newrange = generator(1)
print(next(newrange))  # 0
next(newrange)  # StopIteration


