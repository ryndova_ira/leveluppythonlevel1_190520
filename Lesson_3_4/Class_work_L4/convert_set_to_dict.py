# Функция convert_set_to_dict. Принимает 2 аргумента: множество и значение для поиска. Возвращает
# словарь (из входного множества), в котором ключ=значение из множества, а значение=индекс этого
# элемента в множестве в квадрате, найдено ли искомое значение(True | False) в ключах словаря и
# найдено ли искомое значение(True | False) в значениях словаря, количество элементов в словаре


def convert_set_to_dict(my_set, val):
    my_dict = {}
    for i, k in enumerate(my_set):
        my_dict[k] = i ** 2

    val_in_keys = True if val in my_dict.keys() else False
    val_in_vals = True if val in my_dict.values() else False

    return my_dict, val_in_keys, val_in_vals


def main():
    my_set = {1, 0, False, 2, 3, True, 999, 'my'}
    print(my_set)
    print(convert_set_to_dict(my_set, 3))
    print(convert_set_to_dict(my_set, False))


if __name__ == '__main__':
    main()