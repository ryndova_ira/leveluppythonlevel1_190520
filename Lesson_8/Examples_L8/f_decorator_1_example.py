# ---------------------------------- Создадим свой декоратор "вручную" ------------------------------------------------
def my_decorator(function_to_decorate):
    def the_wrapper_around_the_original_function():
        print("Я - код, до вызова функции")
        function_to_decorate()  # Сама функция
        print("Я - код, срабатывающий после")

    return the_wrapper_around_the_original_function


def constant_function():
    print("Я функция, которую ты не изменишь!")


print("\nconstant_function:")
constant_function()
# Я функция, которую ты не изменишь!

decorated_function = my_decorator(constant_function)

print("\ndecorated_function:")
decorated_function()
# Я - код, до вызова функции
# Я функция, которую ты не изменишь!
# Я - код, срабатывающий после

my_decorator(constant_function)()

constant_function = my_decorator(constant_function)
print("\ndecorated constant_function:")
constant_function()
# Я - код, до вызова функции
# Я функция, которую ты не изменишь!
# Я - код, срабатывающий после


# ---------------------------------- Используем декоратор Python ------------------------------------------------------
@my_decorator
def another_const_function():
    print("Меня тоже нельзя менять!")


print("\ndecorated another constant_function:")
another_const_function()
# Я - код, до вызова функции
# Меня тоже нельзя менять!
# Я - код, срабатывающий после
