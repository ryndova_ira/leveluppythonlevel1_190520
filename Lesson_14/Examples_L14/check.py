from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# webdriver.Chrome('/путь/до/драйвера/chromedriver')
# (chromedriver.exe для Windows) должен находиться в той же папке, что и этот скрипт!
driver = webdriver.Chrome('chromedriver')

driver.get("http://www.python.org")  # Перейти по ссылке
assert "Python" in driver.title  # Проверка есть ли в заголовке страницы слово Python
elem = driver.find_element_by_name("q")  # найти элемент по id="q"
elem.clear()  # очистить строку поиска перед печатанием
elem.send_keys("pycon")  # отправляем текст в строку поиска
elem.send_keys(Keys.RETURN)  # имитируем нажатие клавиши Enter
assert "No results found." not in driver.page_source  # Проверяем, что по-нашему запросу ничего не найдено.
driver.close()  # закрываем браузер
