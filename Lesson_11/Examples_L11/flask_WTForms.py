from flask import Flask, render_template, request, redirect, url_for, flash
from werkzeug.datastructures import MultiDict
from Lesson_11.Examples_L11.forms import ContactForm

# Передаем имя модуля или пакета
# Это необходимо, так как Flask не знает, где искать шаблоны, статические файлы, и т.п.

app = Flask(__name__)


@app.route('/final/', methods=['get'])
def final():
    return "Thanks!"


@app.route('/contact/', methods=['get', 'post'])
def contact():
    form = ContactForm()
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        message = form.message.data
        print(name)
        print(email)
        print(message)
        # здесь логика базы данных
        print("\nData received. Now redirecting ...")
        return redirect(url_for('final'))

    return render_template('contact.html', form=form)


if __name__ == "__main__":
    # Запуск локального сервера с приложением
    app.debug = True
    app.config['SECRET_KEY'] = 'a really really really really long secret key'

    app.run()
