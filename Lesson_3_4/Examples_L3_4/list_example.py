print('\n--------------------------------------- Introduction --------------------------------------------------------')

squares = [1, 4, 9, 16, 25]
miscellaneous = [1, True, None, int, print, "Hello world"]
print('len(miscellaneous) = ', len(miscellaneous))
# len(miscellaneous) =  6


print('\n--------------------------------------- Indexes and slices --------------------------------------------------')

names = ["Dries", "Romeo", "Kevin", "Eden", "Thibaut", "Jan", "Micky"]

print(names[-4])
# 'Eden'

print(names[:2])
# ['Dries', 'Romeo']

print(names[::-1])
# ['Micky', 'Jan', 'Thibaut', 'Eden', 'Kevin', 'Romeo', 'Dries']

print(names[2:7:2])
# ['Kevin', 'Thibaut', 'Micky']


print('\n--------------------------------------- Operators -----------------------------------------------------------')

names = ["Dries", "Romeo", "Kevin", "Eden", "Thibaut", "Jan", "Micky"]

del names[0]
print(names)
# ['Romeo', 'Kevin', 'Eden', 'Thibaut', 'Jan', 'Micky']

del names[::2]
print(names)
# ['Kevin', 'Thibaut', 'Micky']

names[2] = 'Thorn'
print(names)
# ['Kevin', 'Thibaut', 'Thorn']

print(names + ['Jan', 'Vincent', 'Thomas'])
# ['Kevin', 'Thibaut', 'Thorn', 'Jan', 'Vincent', 'Thomas']

print(names * 3)
# ['Kevin', 'Thibaut', 'Thorn', 'Kevin', 'Thibaut', 'Thorn', 'Kevin', 'Thibaut', 'Thorn']


print('\n--------------------------------------- Details -------------------------------------------------------------')

lst = []
lst.append(lst)
print(lst)  # [[...]]
lst.append(1)
print(lst)  # [[...], 1]
print(lst[0])  # [[...], 1]
print(lst[0][1])  # 1

print('\n--------------------------------------- List Comprehension --------------------------------------------------')

# square = [x** 2 for x in range(10)]
square = []
for x in range(10):
    square.append(x ** 2)
print(square)  # [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]

odds_tmp = [x
            for x
            in range(10)
            if x % 2 != 0]
odds = []
for x in range(10):
    if x % 2 != 0:
        odds.append(x)
print(odds)  # [1, 3, 5, 7, 9]

lst_ex = [
    [int(i == j)
     for i
     in range(3)]
    for j
    in range(3)]
print(lst_ex)  # [[1, 0, 0], [0, 1, 0], [0, 0, 1]]

lst_tmp = []
for j in range(3):
    for i in range(3):
        lst_tmp.append(int(i == j))

lst_pair_ex = [(x, y) for x in [1, 2, 3] for y in [3, 1, 4] if x != y]
print(lst_pair_ex)  # [(1, 3), (1, 4), (2, 3), (2, 1), (2, 4), (3, 1), (3, 4)]

print('\n--------------------------------------- Split and Join ------------------------------------------------------')

print('1,2,3'.split(','))   # ['1', '2', '3']

print("-".join(['1', '2', '3']))    # 1-2-3
